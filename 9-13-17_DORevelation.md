Intro
=====

- Core to being united in the church
    - Otherwise will spend too much "dealing with symptoms"

- Why do we spend so much time on Systematic Theology?
    - Learn what the *whole* Bible says, organized by topics
    - REAP: God directing steps in 2 ways
        - understand the text
        - understand the context
    - Systematic Theology is not the only way to study the Bible
        - need more than one way, but that's beyond our study here
    - Common encounter from skeptics: what does the Bible say about "X"
        - Matching two "seemingly" conflicting points

Doctrine of Revelation
======================

- Not the book of Revelation
- How God has made him self known

Definition: What does the Bible say about how God makes himself known?

- Too many people skip this

- Question 1: Where do we get Truth from?
    - Obvious answer: the Bible
    - Different froms throughout history
        - Jesus's teachings, Red Sea split, burning bush, etc.
    - Story: Santa was truth from parents, shattered later than you'd expect (7th grade)
    - How we live today boils down to where you think Truth comes from
        - something that you *can't* skip over

- Epistimology == the study of knowledge
    - Two realities:
        1. (World) truth is obtained by *discovery* --> human senses and logic
            - I believe my senses are reliable, so I can discover truth
            - If I judge the weight of my measured discoveries to be true, then it is true
            - What I (or other men of discovery) say is true is truth
        2. truth is obtained by *revelation* --> truth in creation is only revealed by God
            - I believe God can make His revelation understandable to man
            - God has revealed Himself to me in Christ through His Word
            - What God says in His Word is true
    - Each reality is mutually exclusive
    - Those in the Bible (+Christians) described by the second
    - God is the one who reveals truth, changes the way you evangelize
        - Can't "argue" someone into faith

- Revelation == "reveal", "uncover", "lay bare"
- Everything in the Bible comes from revelation from God himself
    - all done in *love*

- Knowledge vs. Feeling
    - "I don't study theology, I just love Jesus", assumes knowledge leads to a cold heart (no!)
    - Revelation of knowledge is an invitation to intimacy
        - Sharing new information with someone generates vulnerability, shows highest amount of love
        - Example: wedding night reveals the most ever in a relationship up to that point

- Multiple ways "to know" is translated:
    - 1. "know a fact" --> I know about Bill Clinton
    - 2. "experiential knowledge" --> carnal in nature, was a euphamism for sex by the Greeks
    - God wants the second, as it is far more relational

- 1. God reveals himself clearly, but man on his own (apart from God) *does not* understand God
     and *will not* understand
    - Texts: Psalm  19:1 --> God is revealing knowledge, not discovered
             Romans 1:18 --> God has revealed himself, but man have suppressed truth
- 2. The nature of revelation is God's self-disclosure
    - There isn't anything God reveals to you that is not intimate
    - Never morally neutral
    - Everything revealed is an invitation, because is a person
    - Texts: Matthew 16:13-17 --> Jesus confirms that God revealed himself to Simon Peter (Barjona)
    - God turns the "darwinian" society on its head --> God reveals himself to the weak
- 3. God has made himself known to us because of his great love for us and his glory
    - Story of Noah --> God's glory and man were at odds, story isn't about animals but about how
                        everyone died
    - He loves us: Ezekiel (God's glory on display so that we know he is God)
    - He saves us not beause he can, but because he loves us

- Ultimately we believe that the Bible is true because it has been revealed to us
    - When you read it, it speaks truth to your core, even if you don't like it
    - Text: John 10 (Jesus's followers will hear his voice)
        - profound and troubling --> it's not "their fault"

- Takeaway: profound humility because it's not about what we understand, but how God does all the revealing

- Types of revelation:
    - Natural
        - mode: creation + conscience
        - scope: generally to everyone
        - content: says something "of God", but can't fully see it unless combined with special revelation
        - efficacy: remind you that you are sinful and damned
        - example: "wow, the sun is beautiful, God must exist"
    - Special
        - mode: a personal encounter, a mighty act, encarnation
        - scope: a specific time/person/place
        - content: specific knowledge of who God is
        - efficacy: greater salvation or condemnation
        - example: Red Sea parting

