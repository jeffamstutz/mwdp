Gospel Fluency
==============

- We don't default to the gospel in our lives
    - we are born into the thought that we are defined by what we do
    - fluency is about making the gospel a part of our "native thoughts"

- A sign of faith is in a steady, consistent growth in Jesus
    - there are things we are meant to grow out of
    - John 15:1-2
    - God is never content with where you currently are
    - John 15:8 --> specific fruit mentioned is love
    - Colossians 1:28
    - Ephesians 4:11-16
    - Luke 8:15 --> sanctification is a slow process (patience)
    - It is easier to be "spiritually young"
        - The desire is to have freedom of an adult, but have our Father take
          care of every little thing like we are a child
        - Also easier to pastor young congregations
    - Romans 1:16 --> the gospel is both where power for salvation and
                      sanctification is found
    - 1 Corinthians 15:1-2,10 --> the grace of God is opposed to merit, but not
                                  effort

- The "V" of repentance and faith
    1. recognize sin
        - the "what" of what we are doing wrong
        - the Holy Spirit in us convicts us of our sin
        - you must know the Bible in order to know what sin is
        - sin == disobeying and disregarding God's Word
        - sins may or may not feel bad, some ok things may feel bad
        - Genesis 2:16-17
        - our concept of sin may or may not align with God
        - our negative emotions are contrary to the fruits of the spirit
            - anger vs. gentleness
            - worry vs. peace
            - apathy vs. faithfulness
            - sorrow vs. joy
    2. find idols
        - the "why" of what we are doing wrong
        - it is deeper than just doing the wrong thing, it's about loving the
          wrong thing
        - the root of sin is false worship (trust/treasure) created things
          instead of the Creator
        - Romans 1:25
        - Ezekiel 14:1-6
        - categories of idols (from Tim Keller)
            1. approval --> we want to be liked more by people more than by God
            2. comfort --> we want to experience pleasure more than joy in God
            3. power --> we want to be admired and respected by others more
                         than by God (i.e. influence)
            4. control --> we want circumstances do as we say and desire more
                           than we trust God
        - idols make our joy very fragile (it can disappear quickly)
        - Christianity is the only world view that says we should repent of our
          good deeds (done for the wrong reasons)
            - Luke 15
    3. turn from idols
        - most people want to skip straight from #1 to #6
        - this point requires real life change, which is difficult
        - Psalm 51:1-4 --> confession and repentance
        - confession and repentance are equally important
            - confession == saying what the wrong was
            - repentance == saying God's judgement is righteous against us
        - 2 Corinthians 3:18 --> how we change (focusing on Christ)
    4. Jesus lives perfectly
        - 1 Peter 2:22-24
        - Jesus is our solution to our sin, not ourselves
        - EVERYWHERE I have failed, Jesus has been perfect
        - His perfection is the only way his death has meaning, otherwise he
          just dies for his own sins
            - Jesus never let his circumstances (which may have been less than
              ideal) get in the way of caring for other people
            - ex: Luke 18 --> he is more busy AND more patient than me
    5. Jesus died for our sins
        - Philippians 2:5-11
        - Jesus is held higher than any other name because he went lower than
          anyone else
            - nobody else started in heaven
        - salvation "feels easy" because Jesus already paid for it
        - the more specific we understand our own sin, the more specific we
          understand how Jesus paid for our sins
    6. Jesus's resurrection and promises
        - the resurrection was a sign from God that our sins are actually paid
          for (and always promised)
        - 2 Corinthians 1:20
        - 1 Corinthians 15:10
        - 1 Corinthians 15:54-58
        - this is how we understand that our worship produces action
