Unconditional Election
======================

- definition: Election refers to the determination of God before his creation
              of the world of those who would be saved from their sins through
              faith in him.

- Why talk about election?
    - this issue is mostly a western issue
    - we do because God wants us to talk about it --> it's a major theme
    - Deuteronomy 7:7 --> God's love for us isn't because of our status
    - it is part of our identity
    - Isaiah 44:1

- Acts 13:48
- Romans 8:29-30
- Romans 8:33
- Romans 9:11
- Romans 11:5-7
- Colossians 3:12
- 1 Thessalonians 1:4
- 2 Thessalonians 2:13
- 2 Timothy 2:10
- 1 Peter 1:1-2

- Our praise of God is connected to our election
    - our desire to praise God is one of the ways we know we are saved
    - Ephesians 1:4-5 --> primary praise is our election!
    - our praise of God will always be dwarfed until we understand election

- Everything comes down to 1 of 2 beliefs
    - God's determination comes down to something in us (conditional)
        - God has comprehensive foreknowledge of the future and he can see
          ahead of time who among the mass of humanity when hearing the gospel
          will respond positively to that gospel and be saved
    - God's determination comes down to something in Him (unconditional)
        - God elects people from the foundation of the world unconditionally,
          that is, he does not take into consideration anything about individuals
          who will exist

- 4 reasons why unconditional election:
    1. God is sovereign and is in control of everything
        - Ephesians 1:11
    2. God saves men and women not based on works
        - Ephesians 2:1-9
        - not crazy to pray for my unbelief
        - Romans 9:11-14
        - John 1:12
        - resurrection/birth requires someone else's will
    3. scripture teaches that God must move first to be saved
        - Isaiah 64:6-7 --> nobody would call on God's name alone
        - Ephesians 2:1
        - John 10
    4. scripture also displays God's love in election in unique ways
        - it's not burdensom for those who are saved
        - hope is in God chosing people, not people chosing God
        - Romans 8:28-30

