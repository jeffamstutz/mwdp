Doctrine of the Trinity
=======================

- Nobody can *fully* explain it, but that doesn't mean we shouldn't try to
  understand
    - Long discussion throughout church history, with mixed conclusions yet a
      binding doctrine
    - Must understand this doctrine because no other doctrine makes sense apart
      from the trinity

Definition 
----------

- the trinity is that God eternally exists as three persons, Father, Son, and
  Holy Spirit, and each person is fully God, and there is one God
    - God's whole and undivided essence belongs equally, simultaneously, and
      fully to each of the three persons of the Godhead (Halem Sa)
    - No good illustration/example, every single one leads to some sort of
      incorrect doctrine if taken too far

Propositions
------------

1. God is one
    - Deuteronomy 6:4-5
    - Deuteronomy 32:39
    - Isaiah 45:21-22
    - Galatians 3:20
    - Main jewish position (there only can be one God)

2. God is three
    - "Triadic passages"
    - Psalm 33:6 --> plurality? more than just poetry?
    - Isaiah 48:16
    - Isaiah 63:9-10
    - 1 Corinthians 12:4-6
    - Ephesians 4:4-6
    - Acts 2:33
    - Romans 15:30
    - 2 Corinthians 13:14

3. Each person is fully God
    - Genesis 1:1 --> the Father is fully God
    - John 1:1 --> the Son is fully God
        - Jesus didn't always exist, but the Son of God always has
    - John 1:18
    - John 20:28
    - Acts 20:28
    - Romans 9:5
    - John 16:7
    - Acts 5:3-5
    - Acts 16:6-7
    - 1 Corinthians 6:19

4. Each person is distinct from one another
    - Acts 10:19-20
    - Acts 13:2
    - Ephesians 1:1-15
    - 1 Peter 1

5. The three are related to each other eternally

Application
-----------

- God is love, the trinity is about love
    - The center of the trinity is a loving community
    - 1 John 1:8
    - There is no lonliness in God, he didn't created us because of that, it
      was because of love
    - John 17:5
    - John 17:24
