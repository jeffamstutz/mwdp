Spiritual Disciplines
=====================

Abiding
-------

- Fruit is not born out of "good strategy"

- What is it?
    - meanings: "waiting", "staying", "continuing in one place"
    - opposite: "depart", "flee"

- Passages
    - John 6:56
    - John 15:6
    - John 15:7
    - John 15:10
    - 1 John 2:6
    - 1 John 2:10
    - 1 John 2:24
    - 1 John 2:24,27-28
    - 1 John 3:6,9

- What does it look like?
    - Spending time reading scripture
    - Spending time in prayer
    - Spending time in silence and solitude

- Psalm 1
    - Where is blessing not found?
        - Not in the "counsel of the wicked"
        - Not in the "way of sinners"
        - Not in the "seat of scoffers"
    - Where is blessing found?
        - In the delight of the Law
        - In meditation over the Law
            - Being "consumed" by scripture

Prayer
------

- The what, the who, and the why

- What is prayer?
    - We are "friends of Christ", "Bride of Christ", "Children of God" --> All
      pointing to our relationship with God as being a "family"
    - We have a need for relationship
    - John 4:10,19
    - Prayer starts with how God engagued us *first*
    - T. Keller --> prayer is continuing a conversation started with his World
                    and his grace

- Why do we pray?
    - So that the relationship we have with God maintains "sweetness"
    - Luke 18:1
    - 1 Thessalonians 5:16-18
    - Scripture commands that we pray
    - Prayer doesn't change God, but is his way to change us
    - It is our way to surrender to God, realize our "finiteness"

- How do we pray?
    - Many types of prayer: corporate, private, silent, outloud, thanksgiving,
                            praise, request, intercessary
    - Many structures (i.e. Lord's prayer)
        - A.C.T.S. (Adoration, Confession, Thanksgiving, Supplication)
        - J.O.Y. (Jesus, Others, Yourself)
        - Praying through scripture
    - 1 Thessalonians 5
    - God *wants* you to come to him with everything, no matter how raw the
      emotion is

Silence & Solitude
------------------

- The ordinary means for which we grow in our faith

- Mark 1:35-37

- Our hearts and minds are like sponges, we soak up all that's arround us

- Phillipians 4:8

- What prevents us from doing it?
    - "Tyranny of the urgent"
    - Business
    - Fear of being alone

- It is more than just "getting away" or running from people
    - It is far more about reducing the number of voices, so that we can hear
      God more

- Matthew 11:28-30
