Discipleship of the Whole Person
================================

- definition --> every fabric of a person is affected by the gospel
    - more about "not looking magnificent", often its messy
    - Matthew 28:18-20
    - "spiritual parenting" --> expectations need to be appropriate
    - discipleship takes time, often frustrating with repetitive teaching
    - some combination of "coaching" and "teaching", but more than that
    - discipleship is not confined to:
        - current knowledge
        - personality
        - talents or potential
        - perfection of the self first
    - Hebrews 10:14
    - God has chosen to use his people to reach his people
    - discipleship is a "resortation"
    - 4 categories (informed by how sin has destroyed us):
        1. relationship with God
            - Romans 1:18-23 --> sin confuses and rejects knowledge of God
                - examples:
                    - an abusive father == God is that way
                    - nobody dependable in life == scripture can't be either
            - 1 Corinthians 2:10-14 --> we can understand God in ways only
                                        given by the Holy Spirit
            - we are able disciple because we lean on the scriptures
                --> we are allowed to say "I don't know, let's look at
                    scripture"
                --> we tend to focus on replicating people who see things the
                    way we do, rather we must lean on transformations done by
                    the Holy Spirit
                --> we are being discipled as we disciple others
                --> must know more than just doctrine about scripture, rather
                    we must know scripture itself
        2. understanding of self (our identity)
            - we exchanged our identity for a false identity
            - tender subject because it affects the way we view ourselves
            - Ephesians 4:17-19
            - Ephesians 4:20-24
            - discipleship is about helping people live into their new identity
                - the truth of our identity is always true, regardless of our
                  "feeling" or "experience"
            - Jesus gives us a perfect example of living into our identity
            - old self vs. new self:
                - enemy of God vs. son of God
                - slave to sin vs. slave to righteousness
                - hater of God vs. lover of God
                - spiritually dead vs. indwelled by the Holy Spirit
                - powerless against Satan vs. victorious in Christ
            - in the same way you have to exegete scripture, you have to
              exegete one another
                --> have to work hard to study people (we hide things!)
                --> the goal is to have someone own how the Word speaks to them
                --> focus on REAPing scripture
        3. relationship with others
            - bad communication/trust/love/humility/etc.
            - another tender subject
            - sin makes us view each other as rivals (we "size up" each other)
            - it gets in the way of confession (!)
            - 1 John 3:11-12
            - 1 John 3:13-18 --> we regularly have to back to Jesus for the
                                 example of laying down our lives
        4. relationship with the physical world
            - Romans 8:18-23
            - Revelation 21:1-5
            - Revelation 22:1-5
            - we are not the only ones suffering in the current creation right
              now, so we use scripture to guide us through it

- practical applications:
    - ministry vs. discipleship
        - reactive vs. proactive
        - more anonymous vs. personal + vulnerable
        - meets immediate needs vs. transforms lives and success is replication
        - often leads to immediate results vs. painstakingly slow 
    - how did Jesus do this?
        - he was selective
        - he lived among them (hospitality, don't hide your life)
        - he made obedience essential
        - he gave himself (give up some things in life)
        - he led by example (invite them into your life too)
        - he gave them tasks
        - he kept track of progress
        - he expected reproduction

