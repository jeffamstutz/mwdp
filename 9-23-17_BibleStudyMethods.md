Bible Study Methods
===================

- The process given today is large, do not expect to implement everything everyday

- Misconceptions: why we don't study the Bible?
    - "Studying isn't 'my' thing" or "I don't get anything out of it" -->
      studying is a skill, which can be improved
        - It's a process, reward sometimes requires effort over time (ex:
          injury rehab)
    - "Studying the Bible is too 'academic'"
        - Intimacy starts with knowledge --> delight/trust/love in a person
          comes in knowing who they are
        - God reveals himself in the Bible, giving us the opportunity to know
          who he is

Dual Authorship
---------------

- The Bible has 2 authors (types): God and a human
    - 2 Peter 1:20-21 --> no prophecy was spoken by the will of a human
    - 2 Timothy 3:16 --> all scripture is God breathed
    - The *only* place we can confirm that God has spoken is through the Bible

- The *divine* authorship of the Bible means it is true, authoritative, unified,
  important, and won't change or contradict itself (Authority/Inerrency of
  Scripture)

- The *human* authorship of the Bible means we can understand it (Clarity of
  Scripture)

- To understand the Bible, you must understand the human author's intended
  meaning
    - allegory: trying to get extra meanings from Scripture (bad)
    - meaning is defined by the author, not the reader --> exegesis (good)
    - question --> does the meaning I understand account for the human author's
      intent?
    - the Bible wasn't written *to* you, rather it was written *for* you

The Interpretative Journey
--------------------------

1. The original situation and intent
2. Culture/language/time/situation/covenant
3. Principlizing bridge
4. Understanding the passage for our current time/situation/culture
5. Take it to others

- Observation: time with the text
    - Must use enough time --> otherwise we can inject our own meaning
      because we "rush" too quickly
    - We natrually have assumptions that we bring with us, so we must
      guard against them leading us astray
    - "Observations are objective facts that cannot be disputed"

- Interpretation: our attempt at deriving meaning from the text
    - They can be disputed (unlike observations)

- Application

Phillipians
-----------

- Purpose: To promote Gostpel-centered unity (even in the midst of opposition
           and false teaching) for the sake of proclaiming CHrist and advancing the
           Gospel

Why Careful Interpretation is Necessary
---------------------------------------

- It's communication from God to us

- We bring our own biases and prior knowledge into interpretation. "Eisegesis"
  rather than exegesis
    - "I want the text to say what I already believe" --> must keep this bias in mind

- There are many barries to proper interpretation (language, culture, etc.)

- 2 takeaways:
    - Author's big idea: 
        - short statement of the human author's message
        - bound by time
        - communication is from person-to-person
        - process
            1. break up the passage
            2. label each section
            3. create short summary statements of each section (faithful to the passage)
            4. create the ABI by combining your summaries into a concise statement
    - Theological big idea: what does it mean eternally?
        - Short statement of God's redemptive message to mankind of the point in history
        - not bound by time
        - communication is from God to mankind
        - Measure the width of the "river"
            - Find differences
                - personal - what makes you different from the people in the
                             passage
                - cultural --> what differencees are there between their
                               culture and ours?
                - redemptive --> is this passage in a different part of
                                 redeptive history?
            - Find similarities
                - God --> what is true about the tirune God back then and
                          today?
                - Man --> what is true about created, fallen, and redeemed man
                          back then and today?

Tools to Careful Interpretation
-------------------------------

- Content --> observation

- Context --> that which goes before and that which follows after
    - literary: kind of literature (any kind of thing a person writes)
        - History/narrative
        - Parables
        - Poetry
        - Wisdom
        - Prophecy
        - Epistle/Letter
        - Apacalyptic
    - surrounding: zoom out and see what comes before/after
    - historical: when, who, political climate
        - use tools, though know they are not inerrant
            - ESV study bible
            - NIV Zondervan
            - literary study bible (ESV)

- Comparison: using scripture to understand scripture
    - Cross-references
    - Concordances

- Community: church, LTG, MC, cohorts, church history

Why We Skip Application
-----------------------

- We substitute interpretation for application

- We substitute communication for transformation

- We substitute superficial obedience for substantial life change

- We substitute rationalization for repentance

- We substitute an emotional experience for a volitional decision

