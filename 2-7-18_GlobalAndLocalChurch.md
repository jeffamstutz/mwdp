Global & Local Church
=====================

- only way we can fulfill the Great Commission is with others, we can't do it
  by ourselves

- what is the church?
    1. universal church --> "who we are" (identity)
        - Romans 16:5 --> home churchs
        - 1 Corinthians 16:9, 1 Thessalonians 1:1 --> in particular cities
        - Acts 9:31 --> entire geographic regions
        - Ephesians 5:25, 1 Corinthains 12:28
        - visible & invisible
        - The church is...
            - family (1 Timothy 5:1-2, 1 John 3:14-18)
            - Christ's bride (Ephesians 5:32, 2 Corinthians 11:2)
            - building and new temple (1 Corinthians 3:9, 1 Peter 2:5)
            - God's house (Hebrews 3:3)
            - body of Christ (1 Corinthians 12:12-27)
            - nation of priests, people for God's possession (1 Peter 2:5)
        - 4 aspects of identiy
            1. chosen race
            2. royal priesthood
            3. holy nation
            4. people for his own posession
    2. local church --> "what do we do" (activity)
        - "The Church as we ought to experience it in a time and place"
        - Acts 2:42-47 --> see the "impulse" of the early church
        - functions:
            1. The Word is taught, believed and obeyed
            2. Leadership is exercised and enjoyed
            3. genuine care is seen and experienced
            4. the ordinances are observed
            5. The Spirit is poured out in worship
            6. mission is pursued
            7. God is glorified

- structure of the local church
    1. elder-led
        - elder == group of qualified servant leaders, who serve as
                   undersheperds in the local church
        - Titus 1:5-9
        - elders always occur in groups and must be qualified
        - qualifications:
            - male
            - full equality of persons
            - mutual dependence
            - functional distincions
            - above reproach --> "free from accusations"
            - leader at home
            - mature and humble
            - self-controlled, disciplined, and respectable
            - hospitable
            - able to teach
            - sober, gentle, and peacemaking
            - not a lover of money
            - respected by outsiders
        - elders are shepherds
            - Acts 20:28
            - know the sheep
            - feed the sheep
            - lead the sheep
            - protect the sheep
    2. deacon-served
        - Acts 6:1-7
        - 1 Timothy 3:8-13
            - both male and female
        - not "elder lite", rather servents to help elders
    3. congregationally-affirmed
        - Partners affirm and protect the doctrine of the church
        - can't be passive consumers

