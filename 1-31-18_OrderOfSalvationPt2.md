Order of Salvation Pt. 2
========================

- Adoption --> legal act in which believers are made legally adopted children
               of their creator, God
    - Ephesians 1 --> adoption is designed to inspire our praise of God
    - Theological + Relational:
        - Change of status, access, and future
        - Galations 3:26
        - John 1:12
        - Galations 4:1-6 --> we are not slaves, but sons (heirs)
        - Galations 8:13-17
        - Romans 8:13-17 --> "intimate" crying out
        - status --> based on relationship, not work
        - Romans 8:23
        - "sons" get inheritance because in ancient times the first son got it
          all --> that's why it doesn't mention "daughters" here

- Sanctification --> progressive work of God and man that makes us more and
                     more free from sin and like Christ in our lives
    - both a "one time" and "all the time"
    - ongoing work to be "set apart"
    - 2 aspects --> positional and progressive
        - positional --> "moment" of sanctification
            - Philippians 3:20 --> citizenship is in heaven
            - 1 Peter 10:5
            - Ephesians 2:19
            - Colossians 1:13
            - 1 Corinthians 6:11
            - importance --> can't say "can't do anything about sin"
                - no longer under the domain of sin, though we won't be perfect
                  on this side of glory
        - progressive --> "ongoing" process of sanctification
            - where we spend most of our time (prayer, discussion, study, etc.)
            - can be measured by the fruits of the Spirit
            - sanctification is more about realizing how big the gap is between
              God and us, not by us getting closer to God
                - it increases our view of the cross, both its importance and
                  necessity
            - Romans 1-5 --> gospel is recieved, not achieved
            - Romans 6:10-11,13 --> our identity is in Christ
            - Romans 6 --> who you are
            - Romans 7 --> sanctification is a process, always readjusting our
                           view
            - Romans 8 --> no condemnation for those in Christ Jesus
                - no condemnation
                - adopted, not slaves
                - we are Spirit filled
                - God is for us
                - God gives us all things
                - Christ intercedes for us

- Perserverance --> those who are believers are kept by God for the rest of
                    their lives
    - can we "lose" our salvation?
        - Colossians 1:23
        - Hebrews 3:14
        - Hebrews 6:24
            - interpretations: person who falls away is either a) a believer,
                               or b) not a believer
            - these are warnings about people who are not believers, but very
              near Christians
            - possible to "taste" God, but ultimately reject him
            - Hebrews 6:9 talks to other people
        - Hebrews 10
        - Matthew 7:21 --> not all who give lip service will be saved
        - can be "close to the cross", but "far from Christ"
        - Romans 8:1
        - John 10:27
        - John 6:38
        - Ephesians 1:13-14
        - 1 John 5:1-5
        - John 3:16
        - Philippians 1:6
        - so yes, we are always saved, but should be careful to assume who that
          is and thus should use caution when discussing this
            - also be careful to not press condemnation onto someone
            - God has a tighter hold on you than you on him
    - who is really at work with sanctification, me or God?
        - we are to strive for holiness
            - 1 Thessalonians --> flee from sin
            - 2 Peter --> make every effort to grow in Christ like character
            - Philippians 3:12 --> we press on to make it our own
        - the Holy Spirit gives us the ability to be like Christ
            - Philippians 1 --> God will complete the transformation
        - Philippians 2 --> work out your own salvation, both are active
            - God works in us, so our salvation comes out of us

- Glorification --> final step in God's redemptive work of the believer, it
                    will happen when Christ returns and grants all believers
                    from all time, living and dead, perfect resurrection
                    bodies like his own, powered perfectly by the Spirit of
    - Romans 8:17
    - Romans 8:11
    - 2 Corinthians 3
    - impliciations:
        1. righteous standing
            - Romans 5:6-11
        2. we have a holy character
            - Ephesians 5:27
        3. redeemed calling --> new calling
            - 2 Timothy 2:12
            - Romans 8:17
        4. new body
            - Romans 8:23
            - Philippians 3:21
            - 1 Corinthians 15:42-49
                - imperishable, glorious, powerful, spiritual
        5. part of a new creation
            - Romans 8
            - Revelation 21:1-5

