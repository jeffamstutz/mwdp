Communicable Attributes of God
==============================

- 22 attributes (can't do all of them)

- Categores:
    - mental (e.g. invisibility and spirituality)
    - moral (e.g. love patience jealousy)
    - purpose (e.g. will omnicience)
    - summary (e.g. beauty perfection)

- Framework
    1. What does the attribute mean? (observation)
    2. What does the Bible say about the attribute? (interpretation)
    3. Why does it matter? (application)

- God's Knowledge (or omnicience)
    - "God fully knows himself and all things actual and possible in one simple
      and eternal act. God's knowledge is not limited by the creation. He does
      not and cannot learn." --> Grudem definition
    - Job 37:16 --> God is perfect in knowledge
    - 1 John 3:20 --> God is greater than our hearts and knows everything
    - God knows himself
        - only way to know infinitum is the infinite (God)
    - God knows all things actual
        - i.e. all things that exist
        - Isaiah 46:9
        - Matthew 6:8
        - Matthew 10:30
        - Psalm 139 --> God is *magnificent*, and yet wants to know me
    - God knows all things possible
        - knows everything that *could* happen
        - Matthew 11:21
    - Why does it matter?
        - He knows us so deeply
            - He knows us even when we can't exactly express ourselves
            - We cannot escape him (fear of being "found out")
            - Genesis 3 --> Adam and Eve immediately hide after sinning

- God's Wisdom --> perfect application of knowledge
    - What does it mean?
        - "God always chooses the best goals and best means to those goals"
        - Romans 16:27 --> God is the only wise god
        - Job 12:13 --> God's perfect wisdom is coupled with his perfect power
        - Daniel 2:20 --> God gives wisdom, he knows what is in the darkness
    - 1. Wisdom in Creation
    - 2. God's wisdom in his redemptive plan
        - Ephesians 3 --> God's redeptive plan showcases his wisdom
        - James 1:5 --> If you lack wisdom, ask God for it
        - Psalm 111:10 --> If you desire wisdom, fear God
            - Opposite of Aristotle, who said "wisdom comes from knowledge of
              the self"

- God's Holiness (he is separated from us)
    - Isaiah 6:3
    - Holy --> "Wholey pure/healthy"
    - 1 Samual 2:20
    - Exodus 15:11
    - Isaiah 55:8 --> God's thoughts are not our thoughts
        - Not knowing the holiness of God results in not understanding the
          gravity of sin (minimizing it), which also minimizes God's grace
        - Realization of God's holiness creates a "moral shockwave" in us,
          drawing us into worship and near to God (via the cross)

- God's Goodness
    - Can be the most difficult to understand and/or describe
    - Lots and lots of definitions of "good"
    - How do we know that God is good?
        - "the good things in my life" --> dangerous to only define God this
          way, it effectively "tests" God
    - "God is the only standard of good and all God is and does is worthy of
      approval"
    - Luke 18:19
    - Psalm 100:5
    - Psalm 119:68
    - Genesis 1:31
    - 2 Corinthians 1:3
    - Galations 2:5
    - Why does it matter?
        - we have to shift our view of "good" to match God's character
