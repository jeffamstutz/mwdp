The Spiritual Disciplines Part 3
================================

- Acts 2 --> the gospel gets proclaimed, 3000 souls added
    - basically, the church started
    - response to the gospel, which declares the gospel
    - we don't do these things to gain favor from God, we do it as a response

- Service --> using all of yourself to display love and hospitality to meet the
              felt needs of other people wherever you are
    - we serve because:
        1. Christ did
            - Matthew 23:25-28
            - he took the nature of a servant, rather than ruler
        2. we value image bearers of God
            - Phillipians 2 --> we count others more important than ourselves
            - it loosens the grip selfishness has on our heart (humility)
            - Matthew 5 --> this includes our enemies
        3. serving makes us like Christ
            - 1 Peter 4 --> we should use our gifts to honor God, like Christ
            - Romans 12 --> we are collectively Christ's body, to act like him
        4. serving is how Jesus summed up all the scriptures
            - Matthew 23:32-40
    - how do we do this?
        1. look for needs and try to meet them
        2. know your gifts and put them into practice
        3. listen more than you speak
    - John 13:1-5

- Evangelism
    - lies I believe about sharig our faith:
        - nobody cares about what I believe
        - people who are interested in Jesus would pursue him on their own
        - people already know about Jesus and they have rejected him
        - people will on longer want to be my friend if I share my faith with
          them
    - what are the circumstances of evangelism in the past for me?
        - confession of something that I did to someone else, especially when
          they didn't think it was something wrong
        - deflection of a compliment about how "good" I am
    - do I know the gospel when I hear it?
        - ask questions to people to see if they know it
    - 2 Corinthians 4:1-15
    - John 6:44 --> we can't mess up evanngelism
    - adding a time of accountability in MC makes it a more real part of life
    - understanding our identity should generate a desire to talk about Jesus

- Generosity
    - 2 Corinthians 8:1-15
        - 1 Corinthians was a big rebuke of that church, 2 Corinthians is a
          different tone
        - vs. 7 --> they exceled in everything: faith, speech, etc.
        - they are "convincing" now with their actions (via conviction)
        - however, Paul is adament about generocity (specifically financial)
          also in these actions
        - seeing a response to an "act of grace":
            1. you were given everything you have is not because you deserve it
            2. we give graciously because of how we were shown grace
        - Paul describes this "act of grace" in giving, despite they were in
          poverty
        - generosity is proof of the gospel alive in us, an issue of obedience
            - however, we will never be at a place where we feel like we "give
              enough"
            - Paul is using the impoverished here to illustrate the heart
              position, not the amount here
        - can't have a "scarcity" mentality to obey here, must be an
          "abundance" mentality
        - vs. 12 --> giving is about readiness, not amount
            - we are without excuse, often an issue of us even wanting to give
            - God wants to soften our hearts through obedience in giving
    - how do we get there?
        - don't just "go do your budget", danger of doing things on your own
        - instead, read scripture --> God's word is what changes our heart
        - the issue isn't about money, it's about the heart
        - looking at our current budget numbers will be sobering, the flow of
          money doesn't lie
    - Ephesians 2 --> we were dead, but God gave us everything
