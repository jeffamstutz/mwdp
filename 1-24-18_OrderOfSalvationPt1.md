Order of Salvation Part 1
=========================

- Romans 8:29-30

- Calling --> called by God, to God
    - "general call": God's call through a human witness
        - Isaiah 45:22
        - Isaiah 55:1
        - Matthew 11:28
        - John 7:37
        - Revelation 22:17
        - Acts 17:30
        - 4 verses for how to participate in this:
            - Romans 3:23
            - Romans 6:23
            - Romans 5:8
            - Romans 10:9
    - "effectual call": God sovereighnly summns the elect into fellowship with
                        himself
        - Romans 8:30
        - John 6:37,44
        - Acts 2:39
        - 2 Thessalonains 2:14
        - John 11
        - 2 Corinthians 4:6
        - 1 Peter 2:9
        - 1 Corinthians 1:9
        - 1 Corinthians 1:21-31

- Regeneration --> God sovereignly imparts new life in us
    - First effect of effectual calling
    - John 1:13
    - John 3:3
    - Ephesians 2:4-5
    - Titus 3:4-6
    - Romans 3:10-12
    - Romans 8:7-8
    - James 1:18
    - 1 Peter 1:3
    - 1 John 5:1
    - it involves both death and birth (not just a sickness)
        - not "going to church to get a fix to a problem"
    - Romans 6:4-11

- Conversion --> Our willing response to the gospel call by repenting of our
                 sins
    - Acts 3:19
    - Acts 16
    - Romans 10:9
    - faith = understanding + ascent (approval) + trust => life, death, and
              ressurection of Jesus to save us from our sins
    - James 2:19
    - repentance
        - Matthew 4:17
        - Matthew 3:7
        - 2 Corinthians 7:10
        - Psalm 51:4
        - Acts 11:18
        - 2 Timothy 2:24-26
        - Romans 2:4
    - faith
        - Romans 5:1
        - Romans 4:2-3
        - Ephesians 2:8
        - James 2:14

- Justification --> Instantaneous act of God that pays the debt of our sins and
                    declares us right with God
    - Romans 8:33
    - not just making you "morally neutral", but as perfect as Jesus
    - Romans 3
    - 2 Corinthains 5

