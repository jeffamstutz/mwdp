Spiritual Disciplines (part 2)
==============================

Fellowship
----------

- world definition: friendly association, especially with people who share
                    one's interests
    - not a great definition --> a bit too simple

- biblical meaning: greek "koinonia"
    - participation
    - shareholder
    - partnership
    - community
    - contribution

- passages:
    - Acts 2:42
    - Galatians 2:9
    - Phillipians 1:5
    - Phillipians 3:10
    - 1 John 1:3
    - 2 Corinthians 8:4

- what is the primary expression of fellowship?
    - John 13:34-35
    - John 15
    - 1 John 3-5

- primary barriers to fellowship
    - knowing each other --> hard to love people deeply who you don't know well
    - risk of isolation

- practical ways to do fellowship
    - schedule time (e.g. lunch) with people
    - vacationing

- fellowship is a means for perserverance in our faith
    - Hebrews 10:23-27
    - Hebrews 3:12-14

Fasting
-------

- intentionally denying youself of something good for the sake of Godliness
    - renouncing something good for something greater (God)
  
- fasting 1) magnifies Christ, and 2) makes us more like him

- reasons we don't do it
    - uncomfortable
    - medical reasons
    - social consequences
    - counter cultural

- Mark 2:18-20

- OT fasting was a sign of humility and a desperate cry for help

- Matthew 6:16-18 --> fast in secret, not for the approval of man
    - personal/intimate action with God 

- magnifies Christ
    - it proclaims his finished work and expresses our longing for his return
    - Jesus is our portion (not food)

- practical fasting
    - when fasting: pray more (hunger pain drives us to pray)
    - clarity and sensitivity to the Holy Spirit
    - removal from/letting go of the world
    - thankfulness/gratitute
    - intimacy with God

Confession and Repentance
-------------------------

- Psalm 32:1-5

- what is it?
    - confession --> verbal acknowledgement of sin
    - repentance --> act of rejection of sin and movement toward God

- caveats:
    1. not a part of salvation (justification), but for sanctification
    2. not going to talk about the relational part of these things
        - will look at this wrt God, not each other

- God is sinned against, first and foremost, where we sin against his personhood

- James 5:16

- 4 components of confession and repentance
    1. grieve --> we should feel the disturbing nature of our brokeness
        - it is good and Godly to feel this, different from worldly grief
          (which is hopeless because it's based on ourselves)
        - this is evidence that we are Christian
        - requires the "weightiness" of sin (does it still bother me?)
    2. admit --> verbally acknowledge sin
        - again, not part of salvation but for sanctification 
        - reminds us of the personhood of God
        - be specific: not be cause God needs us to, but for our own awareness
            - more detail --> more hatred of sin
        - should be slow and steady, not trying to move past it too quickly
        - Psalm 51 --> David's confession
    3. reject --> putting repentance into action
        - Ephesians 2 --> we become co-laborers in our sanctification
        - need to be creative and remarkably intentional
        - can't view sin as something that can be "danced with", but must be
          destroyed
        - "war mentality"
    4. rejoice --> sense of hope and thanksgiving because our sin isn't the end
        - we are not owned by sin anymore (even when we feel like it does)
        - this only happens last, requiring the first 3
        - only felt when we don't side-step grief

- practical items:
    - write down confessions
    - practice it with one another
    - decreasing amount of time between rebellion and confession
