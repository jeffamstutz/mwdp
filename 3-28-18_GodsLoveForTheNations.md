God's Love for the Nations
==========================

- not a minor doctrine, rather a major thread throughout the entire Bible

- Genesis 1:28
    - "image of God" --> image of a ruler, that wherever the image goes there
                         is a proclamation of God's reign
- Genesis 3 --> the fall
- Genesis 9 --> Noah (be fruitful and multiply)
- Genesis 12:1-3 --> Abraham (will make a great nation)
- Exodus --> plagues represent domination of false gods of Egypt (God reigns)

- ~1600 passages in the Bible which talk about God's heart for the nations

- lots of struggles with the feeling of "we shouldn't give because things are
  not great here"

- Psalm 67 --> not just about us, everything given to us is for everyone

- the scope of God's kingdom is the entire globe

- Habbakuk 2:14 --> "the earth will be filled with the knowledge of the glory
                    of the Lord as the waters cover the sea"
    - a verse of encouragement about the "big picture" (God's plan)

- a bit of a strong, but true, statement --> "If you find the Christian life
                                              boring, you're not doing it
                                              right."
    - being blessed in order to be a blessing is our only hope for joy
    - the scope of our ministry is global

- Mark 11:17 --> "Is it not written, My house shall be called a house of prayer
                  for all the nations?"
    - not just about people being monetarily robbed, but also they were robbing
      God of his glory among the nations

- Matthew 24:14 --> the gospel will be proclaimed among the whole earth
    - debate about when this will happen, but scope is clear
    - this is why we are here --> to join God in him claiming people from all
                                  tongues and nations for himself

- Matthew 28 --> the great commission, we are to make disciples by baptizing
                 them and teaching them

- Revelation 5:9 --> God always gets what he paid for
