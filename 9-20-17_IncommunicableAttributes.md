Incommunicable attributes of God
================================

- Things we can't comprehend about God
    - By definition, they are the things that are above us and
      our understanding

- Bible is about God: easy to forget that when we read it --> it's not about *us*
    - Our view of God is very telling about our heart

- 5 we will study tonight: independence, immutability, eternity, omnipresence, unity,

- Indepencence: God is self-sufficient --> God does not *need* us or the rest of creation
    1. He has no needs: emotionally, physically, etc.
        - He loves us anyway, even though he doesn't need/get anything from us (we don't change him)
        - Passage: Exodus 3 --> God describes himself as being independent (when sending Moses to free the Israelites)
        - Passage: Acts 17:24-25 --> God isn't served by us as if he needs us to serve him
        - Passage: Job 41:11 --> "Everything is God's"
        - Passage: Hebrews 6:13 --> God makes a promise on his name, which is the most valueable thing
    2. We and the rest of creation can glofiy him and bring him Joy
        - We are valuable because God says that we are valuable, not because of our "fruit"
        - Passage: Isaiah 43
        - Passage: Ephesians 1:11-12 --> we have an inheritance for us, being predestined according to his purposes
        - Passsage: Zephaniah 3:17 --> God rejoices over us with shouts of joy

- Immutable: God is unchangeable
    1. God is unchanging in his being, i.e., his perfections and promises
        - Passage: Psalm 102:25-27 --> things wear out, but God does not
        - Passage: Malachi 3:6
        - Passage: James 1:17 --> every perfect gift is from God, who has no variation
        - "The contrast between being and becoming marks the difference between the Creator and the creature."
    2. God is unchanging in his purposes or promises
        - If God could change from good to bad, which would be AWEFUL for us
        - Passage: Psalm 33:11 --> God's plans continue on generation after generation
        - Passage: Isaiah 46:9-11 --> God remembers the things long past, and continues his plans into the future
        - God "changes his mind" in scripture (ex: Jonah w/ Ninevah)?
            - Passage: Exodus 32 --> golden calf generates wrath, Moses pleads for him to not do that

- Eternal: God is outside of time
    1. God is timeless in his being
        - He can both overlook time and participate in it
        - Not bound by the past (as we can be)
        - Passage: Psalm 90:2 --> God was himself before any of creation
        - Passage: Revelation 1:8 --> God is the beginning, the middle, and end
        - Passage: Genesis 1:1 --> God existed to create the heavens and the earth
        - Passage: John 8:58 --> "Jesus said 'before Abraham was, I am'"
    2. However, God works in time
        - Passage: Galations 4:4-5 --> God redeemed the earth (sending Jesus) at a particular moment

- Omnipresence: God is equally present in all of time
    1. God is the creator of and lord over space
        - God does not have size or spatial dimensions, but is everywhere equally
        - Passage: Genesis 1:1-2a --> God was present in creation (+Collosians 2)
        - Passage: Deuteronomy 10:14
        - Passage: Jeremiah 23-23-24 --> God is way more than "nearby"
        - Passage: Psalm 139:7-10 --> where can someone go to get away from God?
        - There are no "secret sins", they are done in the full presense of the trinity
        - No unseen sin, and no unseen righteousness

- Unity: simplicity
    1. God is not divided into parts (like the physics of light!)
        - Passage: Exodus 34:6-7 --> God is both "kind" and "just"

Takeaways
---------

- How do these attributes help us understand who God is?
    1. They expose our "silly" idolitries
    2. Gives us true freedom from need of control
    3. Recognizing these things inspire right worship
    4. They encourage us by God's perfect love --> God's amazing being is the one who came after us
