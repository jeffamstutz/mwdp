The Holy Spirit (Doctrine of Numatology)
========================================

- The Father is always above creation (transcendant God)
- The Son (Jesus) came to dwell among us
- The Holy Spirit came to dwell inside us

- John 14:13-17

Who is the Holy Spirit?
-----------------------

- attributes of the Holy Spirit:
    - 1 Corinthians 2:10-11 --> the Spirit searches all things and knows the
                                thoughts of God
    - Ephesians 4:30 --> the Spirit can be grieved
    - 1 Corinthians 12:11 --> the Spirit distributes gifts
    - Galatians 5:22-23 --> the fruit of the Spirit

- what the Holy Spirit does:
    - John 14:26 --> he teaches
    - John 15:26 --> he bears witness
    - Romans 8:24 --> the Spirit leads or guides
    - Romans 8:26 --> he intercedes for us as Christ also
    - Acts 13:4 --> he directs human activities

- how is the Holy Spirit treated:
    - Acts 5:3 --> he was lied to
    - Acts 7:51 --> the Holy Spirit can be resisted
    - Hebrews 10:29 --> he can be insulted
    - Matthew 12:31

- do we treat the Holy Spirit as a person?
    - do we ask him to teach us or whoever is preaching?
    - are we lead by him or by our own spirit?
    - are we strengthed when tempted to know he is praying for us?

- the diety of the Holy Spirit:
    - Acts 5:3 --> lied to God
    - 1 Corinthians 3:16 --> God dwells in us
    - 2 Corinthians 3:16 --> the Lord is the Spirit
    - Acts 7 --> words spoken by God in the OT are words from the Holy Spirit
    - Acts 28 --> the Holy Spirit spoke through Isaiah
    - Hebrews 3
    - Heberws 10:15-17
    - incommunicable attribures
        - Hebrews 9:14
        - Hebrews 9
        - Psalm 139
    - Genesis 1:2
    - John 3:5-6, Titus 3:5 --> regeneration
    - 1 Peter 2 --> santification is a work of the Holy Spirit

- the Holy Spirit came (John 16:14) to show us the work of the Son

- triadic passages
    - baptism of Christ
    - the great commision
    - 2 Corinthians 13:14 --> God with us through the Holy Spirit

- the work of the Holy Spirit:
    - role in the OT
        - he came upon the judges, prophets, craftsmen, and leaders
        - he empowered ordinary men to do extrodinary things
        - judges --> deliver people from oppression
        - prophets --> boldly and accurately speak the Word of God
        - craftsmen --> artistic and skillful worship through vocation
            - Exodus 31:1-5
        - leaders --> help carry the burden of a group of people
            - Numbers 11:16-17,24-25
        - work was always in obedience, never as an emotional response
    - role in the ministry of Christ
        - Isaiah 61 (quoted in Luke 4) --> spirit is in Jesus, fulfilling
                                           scripture
        - Jesus was concieved by the power of the Holy Spirit
        - Jesus's work was empowered through the Holy Spirit
            - Jesus did his work through his humanity, which required the Holy
              Spirit to work
        - Romans 8:1-11
        - we have permenant access to the Holy Spirit (not true in OT)
        - Jesus didn't come just to save us, but to save us so that we can
          worship God (forshadowed by deliverance in Exodus)
        - Acts 10:38 --> Jesus was anointed with the Holy Spirit
    - role in the church
        - mentioned more in the NT than OT (2.5x more mentions vs. 0.25x size
          of text)
        - ability to be like Christ because of the Holy Spirit coming in Acts
        - Matthew 28:18-20
        - Acts 1:7-9
        - Genesis 11:4-8

- Matthew 12:31-32 --> eternal peril for those who reject the Holy Spirit
