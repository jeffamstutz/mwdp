God's Providence
================

- the Doctrine of Providence is meant for relief, not theological academics

- life is made up of "ordinary moments", where both the smallest and biggest
  things happen
    - God is at work in all of them --> our experience cannot determine our
      doctrine
    - we cannot decide when doctrine does or does not apply
    - we cannot change who God is, even when we are suffering
    - Romans 11:33-36
    - Deuteronomy 29:29
    - we are entirely dependant on revelation, the topic is HOLY

- framework:
    1. God's glory is worth everything
        - God's glory is above our understanding
    2. God's character can be trusted
    3. God's design for your sanctification is best 
    4. God's sovereignty, Man's responsibility
        - seemingly in contradiction, but they ultimately work together

- definition: God is continually involved with all created things in such a way
              that he...
    1. (preservation) keeps them xisting and maintaining ithe properties with
       which he created them
    2. (concurrence) cooperates with created things in every action, dircting
       their distinctive properties to cause them to act as they do
    3. (government) directs them to fulfill his purposes

- preservation --> God holds everything together
    - Hebrews 1:2-3

- concurrence --> everything is being worked by God, nothing left out
    - Ephesians 1:11

- government --> all things are directed for his purposes
    - Psalm 119:91
    - Romans 8:28
    - God is omnipatent --> God has "all strenght", not just "the most
                            strength"

- God's decrees --> the eternal plans of God whereby, before the creation of
                    the world, he determined to bring about everything that
                    happens
    - God's decrees are the "blueprint" that his providence is working out
    - Psalm 139:16
    - "dual causality" --> the divine primary cause and creaturely secondary
                           cause events are fully caused by God and fully
                           caused by the creature
    - Psalm 104:21
    - 1 Corinthians 4:7
    - Proverbs 16:9 (many in ch. 16)
    - 1 Samuel 2:25
    - Acts 17:26

- God's providence in all things, including evil
    - natural evil --> any evil in our world which is not moral
        - natural disasters, sickness, seemingly random tragedy
        - Genesis 50:20 -- > God didn't "indend evil", rather was working good
        - Acts 4:27-28
    - moral evil --> when morally responsible creatures do what is wrong

- two biblical truths in tension:
    - God's absolute sovereignty
        - if you only see this truth, then you get "fatalism" --> we don't do
          anything on our own
    - human responsibility
        - if you only see this truth, then you get "libertarian free will" -->
          the will is determined by nothing, not even God

- God controls/wills/ordains/makes/creates/causes evil
    - Genesis 50:19-20
    - Genesis 37:28
    - Genesis 45:5,7-8
    - Psalm 105:17
    - Job 2:10
    - Deuteronomy 32:39
    - Exodus 4:11
    - Isaiah 45:7
    - Amos 3:6
    - Acts 4:27-28
    - Isaiah 53:10
    - ...in such a way that God himself isn't evil
    - analogy --> evil is like a knife (bad, hurts people), in the hands of a
                  surgeon (someone who uses a knife to fix people)
    - Romans 9:19-23 --> we don't understand God's will all the time, it's not
                         our place to tell God something he does is not OK

- human freedom and culpability
    - 5 mysteries
        1. creation out of nothing
        2. the two natures of Christ (fully God and fully man)
        3. the dual authorship of scripture (both God's words and mans words)
        4. the trinity
        5. responsibility and God's sovereignty
    - compatibilism --> absolute divine sovereignty is compatible with human
                        significance and real human choices
        - a person's actions are free and responsible when:
            1. they are not externally coerced
            2. they are chosen voluntarily, according to internal desires
            3. they are the cause of real effects (i.e. not a dream)
        - compatabilist free-will --> we are aware of no restraints on our will
                                      from God when we make decisions...we make
                                      willing decisions that have real effects
        - libertarian free-will --> the ability to make choices that are not
                                    determined by God (or anything)
    - a person's actions are free and responsible when:
        1. no other cause can determine of the outcome
        2. multiple options (A and non-A) were equally possible choices at the
           time of choosing
    - the problem of foreknowledge and Open Theism
        - open theism --> the belief that God does not and cannot have certain
                          knowledge of the future choices of truely free people

- systematic theology vs. pastoral theology
    - systematic theology --> what I believe must be true
    - pastoral theology --> what should I say about what is true
        - sit and listen, then decide what to say (sometimes "I don't know")

- the problem of evil (two problems: philosophical and personal)
    - how can a loving God allow evil and suffering?
        - the philosophical objection --> the rational problem
            1. God is good and must be willing to destroy evil
            2. God is all-powerful and must be able to destroy evil
            3. evil exists
            4. God cannot exist
        - David Hume (1711-1776)
            1. is God willing but unable to prevent evil? Then his is without
               power
            2. is He able but not willing, then he's evil
            3. is He both able and willing? Then where did evil come from?
            4. if He's good, he's not powerful. If He's powerful, he's not good
        - Mackie (1917-1981)
            1. if God exists, then he must have a good reason for evil and
               suffering
            2. there's lots of evil around
            3. there's no good reason that we have discovered for evil's
            4. therefore, God does not exist
    - failed attempts to answer the problem:
        - punishment theodicy --> suffering exists because we're evil and it's
                                  our punishment
        - free will theodicy --> suffering exists entirely because we are free
                                 to choose good or evil, God doesn't determine
                                 choices so that we may be free
        - natural law theodicy --> for there to be order and regularity in
                                   nature, choices must have consequences
    - don't try to insert specific reasons for evil events!
    - Job's friends trivialized his suffering by trying to map it to Job's
      life, which God condems them for it
    - the philosophical answer:
        1. God is good and can allow evil if there is a morality sufficient
           reason for its existence
        2. God is all-powerful and can destroy evil
        3. Evil exists under the sovereignty of God and only exists because God
           has willed that it exists for His purposes until He finally destroys
           it
    - it's all for God's glory! --> a somewhat difficult to understand
                                    explanation, but the only one the Bible
                                    gives us
    - the emotional objection --> the personal problem
        - this objection is the most pursuasive
        - pastoral response --> show up and shut up!
    - ultimate defence of Christianity --> can't blame God for evil because he
                                           lived among it too (an suffered
                                           because of it)
        - Hebrews 2:17-18
        - suffering has purpose, and God loves us (with covenant through
          Christ)
        - 2 Corinthians 1:3-7

- counseling in the midst of a broken world
    - we counsel and help others dealing with the problem of evil and suffering
      knowing that from the beginning the problem of evil has an expiration
      date
    - the experience of evil and suffering should feel like an ultimate
      violation fo who we are, an alien experience that we were not equipped to
      handle
    - evil and suffering in this world is at a minimum meant to remind us of
      our desperate need of God in light of how catastrophic sin is - not just
      moral sin
    - 10 things to remember:
        1. approach the situation as a learner/helper instead of a fixer
            - the only "fixer" is Jesus
            - we are merely tools in the hands of Jesus in which he works, not
              us
            - it helps our definition of what "success" is, we can't "fix"
              someone else
            - Jesus should work out the time table of work
            - Proverbs --> way of a man that seems good that leads to death
        2. keep in mind who is in control
            - Philippians 1:6
            - Romans 8:28-30
            - remember who the perfect counselor is (Holy Spirit)
        3. be careful of assuming you fully understand what they're going
           through
            - don't say "I completely understand" --> not possible!
            - otherwise, we take what functionally works for us and apply it to
              someone else, assuming it will work
            - there's always more "story" or "layers" which we will not see
            - even Jesus healed people in different ways for different people
              at different times
        4. be careful to not minimize the suffering and difficulty the
           individual is dealing with 
            - the impact of suffering and evil in our lives is not so much
              measured by the actual event itself, but rather by how much that
              event has devastated our everyday normative experience and
              expectations in life
        5. be careful about identifying specific purposes of evil and suffering
           a person is going through
            - easy to fall into a trap by using Romans 8:28 too quickly
            - a specific purpose that God uses suffering should not overshadow
              the rest of the consequences of evil (suffering could still be
              ongoing)
        6. speaking truth in love doesn't mean unloading all the truth we have
           in the moment
            - be sensitive about when and why to communicate certain truths
            - Matthew 16:5-12 --> Jesus models this, "you cannot bear them now"
            - Proverbs 27:14 --> context of feedback matters, don't feed
                                 "theological steaks" to "spiritual infants"
        7. understand the difference between expressions of felt truth vs. what
           they know is true
            - Lamentations + Psalms --> "ups and downs", feeling vs. knowledge
            - Lamentations 3:1-24 --> lots of lament (emotional), but he knows
                                      in his soul who God is anyway (v. 21-24)
            - Romans 12:15
        8. offer God's comfort
            - 2 Corinthians 1:3-4 --> no comfort outside of God, in any
                                      affliction
            - God often provides comfort _in_ suffering, not just taking away
              suffering
        9. be careful not to offer false hope, rather bank on the clear
           promises of God
            - false hope most likely comes in the beginning, when someone is
              most dispared
            - can't just say "it will get better", scripture says nothing about
              guarenteeing that suffering will be taken away, rather it says he
              will help us endure suffering
        10. lean into God's character and the hope he has given us
            - our posture toward God's character shapes our direction either
              toward or away from him
            - trust in God often doesn't come from a better understanding of
              the situation, rather it's who God is that gives us faith and
              hope

