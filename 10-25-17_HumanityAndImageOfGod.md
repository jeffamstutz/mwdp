Image of God
============

- We have inherent value because we are made in the image of God

- 4 basic questions about humanity:
    - origin: where did I come from?
    - meaning: why am I here?
    - morality: how should I live?
    - destiny: where am I going?
    - The world does not truely understand how to answer those questions
        - "People are just like nature"
        - "Humans are super important, making them deity-like"
    - The Bible provides answers to all of them

Why did God create humanity?
----------------------------

- Genesis 1:26-31
    - Rhythm to the creation, but a break in the pattern when humanity is
      created
    - 5 distinctions:
        - final creation
        - only man and woman made in the image of God
        - only ones given dominion over the Earth
        - prior to the creation of humanity, alone was there divine councel
        - only ones stated to be male and female

- What is the chief end of man?
    - Man's chief end is to glorfiy God, and enjoy him forever
    - Isaiah 43:1-7
    - Ephesians 1:11-12
    - I Corinthians 10:31
    - Psalm 16:11

- What does it look like to "glorify God"?
    - Obedience in all ways

How did God create humnaity?
----------------------------

- Answer: in the image of God
    - Genesis 1:26-27 --> repetition used to ephasize importance
    - "Humanity is like God and represents God"

- Humanity was unquely created to:
    - relate --> relational capacity (from the trinitarian nature of God)
        - Genesis 5:3
        - Genesis 2:22-25
    - rule --> ability to lead in creation, we have a purpose
        - Genesis 1:28
        - Genesis 2:15
        - Genesis 2:19
        - God told Israel not to make images (e.g. statues of Kings) because
          _we_ are the image of God, not anything else
    - reflect --> we represent God's nature
        - 1 Peter 2:12
        - Similar to the moon (doesn't have it's own light, but simply reflects
          the sun)

- Why do we struggle with doing what we are created to do? Sin!
    - Genesis 3:1-13 --> note how Adam blamed Eve, he didn't own up to his sin
    - Romans 1:21-22 --> people traded the glory of God for images of morals
    - end of Genesis 3 --> people cast into the world, but clothes them via the
                           death of an innocent lamb (Christ!)
    - 2 Corinthians 4:4
    - Colossians 1:15
    - Hebrews 1:3
    - Colossians 3:10
    - 2 Corinthians 3:18
    - Romans 8:29

Why does it matter that God created humanity?
---------------------------------------------

- Categories for why the image of God matters:
    1. The Imago Dei means that all human life is precious and has dignity
        - James 3:9
        - Genesis 9:6
    2. God's image is connected to his creation of male and female
        - Gender is real and not without meaning
        - God uses gender to create the concept of marriage, a uniquely tied
          relationship
    3. Implications of the image of God for our own self-image
        - value is higher than the rest of creation, because God said so

