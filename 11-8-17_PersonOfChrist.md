Person of Christ
================

Why is styding Christ important?
--------------------------------

1. Studying and knowing the Person of Christ is an end of itself in how we are
   created
    - most people study something as a means to and end
    - Colossians 1:15
    - we are designed to cherish God and want to know Christ
    - John 1
    - we study Christ soe we can see God
    - Hebrews 1:1-3
    - God coming to earth is the most perfect revelation we can have
    - If you want to love Christ, you have to know him
        - You can't love something that you don't know
    - John 17:3
    - Heaven is about being with Christ, not some "utopia" that we think up

2. Studying Christ is essential to the Christian life
    - Fighting temptation
        - Hebrews 4:14
        - Christ is the only one who can resist temptation, he faced it
          himself and always won
        - We can't fully comprehend how big his temptations were
    - Pursue sanctification
        - 2 Corinthians 3:18

3. Studying Christ is one of the primary evidences of saving faith
    - You can't be ignorant of the person of Christ and be saved
    - Psalm --> you love Jesus or you die
    - People who are saved have a drive to know Jesus

Jesus Christ is fully God
-------------------------

- offensive to other cultures and belief systems

- can't be a prophet, teacher, or moral person if he isn't God
    - otherwise he'd be a liar

- Defense:
    1. The Bible applies names of God to Jesus
        - 'Theos' in Greek
        - John 1:1,14
        - 1 John 5:20
        - John 5:18 --> blasphemy of claiming to be God was the offense
        - Hebrews 1:8 --> God says that the Son is God
        - Matthew 24:41-46
        - Daniel 7
            - "Son of Man" is a famous title from Daniel, which Jesus
              calls himself
            - Matthew 26:62-66
    2. Jesus intends for his disciples to worship him as God
        - Matthew 28:16-17
        - John 20:28
        - John 5:22-23
        - John 8:58-59
        - John 10:30
        - John 17:5
        - John 10:27-28
        - Mark 2:5-7

Jesus Christ is fully human
---------------------------

1. The Bible claims he is human
    - 1 John 4:1-3
    - Isaiah 7:14
    - Isaiah 9:6
    - Micah 5:2-3
    - Isaiah 53:2-3

2. His life indicated his humanity
    - Luke 2:40,52 --> teachings were learned

- Where does his humanity intersect with his divinity?
    - could he sin? (no, don't have to sin to be human)
        - we are actually "sub-human" because of sin
        - we have redefined the norm with what is broken
            - "redefine humanity in our own image, not God's"

- Implication: Jesus is forever our human --> he is a great high prophet
    - Permanence
        1. as a prophet
        2. as the High Priest
        3. as the great King
            - Luke 1:32-33
