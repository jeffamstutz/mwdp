Mercy And Justice
=================

- 4 stages of God's redemptive story:
    - creation
    - fall
    - redemption
    - restoration

- layers of ourselves (in to out):
    1. spiritual
    2. emotional
    3. social
    4. physical
    - the above list of needs to be met are in reverse order of ease to meet
    - bearing other peoples' burdens requires direct support of their need
        - not all ways of helping are "burden bearing"

- wrong motivations for mercy:
    - shame/guilt driven decisions --> not really mercy
        - not just "feeling bad" for them
    - fear
    - pride --> "I am awesome for helping"

- Luke 10 --> the good Samaratin
    - ways he was merciful
        - stopped what he was doing
        - bandaged his wounds
        - took him to safety
        - paid for his care
        - came back to check on him
        - helped an enemy of himself (culturally acceptable to hate)
    - Jesus uses an example of an extreme situation as a "litmus test" to see
      if faith is genuine
        - the question was basically "how low of a bar is it to get to heaven?"

- core tension with mercy: unconditional vs. conditional giving
    - unconditional giving
        - Matthew 5:38-42 --> if someone asks for help, "just do it"
            - not very practical, seems like an unreasonable request
            - the point is that it isn't possible, sin gets in the way
            - our default position, however, should be "yes I will give"
    - conditional giving
        - the only time to withold giving is when it is less loving to give to
          them
        - the only real way to know what is loving to give to someone is to
          know them
    - complications:
        - avoidance
        - self righteous
        - unknown circumstances

- definition of poverty (group):
    - lack of basic needs
    - lack of resources
    - hopeless
    - broken --> not fulfilled
    - cyclical
    - needy
    - no way of of situation (no opportunity)

- definition of poverty (by the poor):
    - low self-esteem
    - not feeling needed
    - lonely
    - feeling useless
    - depressed
    - isolated

- when do we limit mercy?
    - relief vs. development
        - relief is 1-way, meant to alleviate something
        - development is 2-way, help someone help themselves
            - 2 Thessalonians 6:13-15 --> "cop out"
    - relief and development, chosing development is dependant on the ability
      for someone to do something for themselves
        - requires knowledge of the person
        - the "mercy line" is balancing the two

